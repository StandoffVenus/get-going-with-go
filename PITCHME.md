@title[Title]

@snap[north text-huge text-green]
Get Going with Go
@snapend

@snap[south text-normal text-white]
Presented by Liam Mueller
@snapend

---

@title[What is Go?]

### What is Go?
@ul[no-dot-list]
- <blockquote class="quote-text"><i class="fa fa-quote-left" aria-hidden="true"></i> Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.<br><span class="quote-attribution">- Google</span></blockquote>
- <blockquote class="quote-text"><i class="fa fa-quote-left" aria-hidden="true"></i> Go is a statically typed, compiled language in the tradition of C, with memory safety, garbage collection, structural typing, and CSP-style concurrency.<br><span class="quote-attribution">- Wikipedia</span></blockquote>
@ulend

Note:

- Google definition sucks

---

@title[What is Go? (continued)]

### That means...
@ul
- Similar to the C/C++ family
- Has C-esque capabilities, but much safer @note[Safe pointers, garbage collection]
- Concurrency built-in @note[Many object-oriented languages are built thread-safe first]
@ulend

---

@title[Where is Go Used?]

### Where is Go used?
@ul
- Networking/Web servers
- CLI's
- Docker - libcontainer
- Distributed computing
- Database engines @note[Relational and NoSQL]
- VM's
- Interpreters/Compilers @note[Lots of Lisp]
- Scientific computing
@ulend

---

@title[Disclaimer]

##### Disclaimer: Go may not be for you if you have

@ul
- used reflection
- used generics
- used scope modifiers
- exercised DRY
- written code
@ulend

---

@title[What Does Go Look Like?]

### What does Go look like?

```go
package main

import "fmt"

func main() {
	fmt.Println("Hello, 世界")
}
```
@[1](Package declaration)
@[3](Imports)
@[5](Function declaration)
@[6](Import usage and output)
@[1-7]

@ul[no-dot-list]
- Test out your own Go: https://play.golang.org/
@ulend

---

@title[Variables]

### Variables

```go
var foo int = 45
var bar = 45
baz := 45
```
@[1](Explicit declaration)
@[2](Type inference)
@[3](Shorter type inference)

---

@title[Constants]

### Constants

```go
const (
    BUFFER_SIZE = 40
    PERM_STRING = "text"
)
```

---

@title[iota]

### `iota`

```go
const (
    a = iota // -> 0
    b        // -> 1
    c        // -> 2
    d        // -> 3
)
```

---

@title[Enums]

### Enums

```go
const (
    A HttpStatusCode = iota + 200 // -> 200
    B                             // -> 201
    C                             // -> 202
    D                             // -> 203
)
```

---

@title[Built-In Types]

### Variables

##### Built-In Types
@ul
- bool
- string
- int / int8 / int16 / int32 (rune) / int64 @note[Explain runes]
- uint / uint8 (byte) / uint16 / uint32 / uint64 / uintptr @note[Explain byte alias]
- float32 / float64
- complex64 / complex128 @note[Can do some crazy math]
@ulend

Note:

- Prepare to explain uint = unsigned

---

@title[Defaults]

### Variables

##### Defaults
Upon declaration (without explicit value)...
@ul
- All numeric types are initialized to 0
- All booleans are false
- All strings are empty
@ulend

---

@title[Arrays]

### Arrays

```go
var array [5]int

anotherArray := [4]int { 3, 4, 4, 5 }

yetAnotherArray := [...]int { 2, 3, 4, 9 }
```
@[1](Zeroed array)
@[3](Array literal)
@[5](Array literal omitting length)

---

@title[Slices]

### Slices

```go
someSlice := []int { 3, 4, 5 }
```

---

@title[Slices from Array]

### Slices from Arrays

```go
array := [3]int { 2, 3, 4 }
slice := array[1:3]

fmt.Println(slice) // -> [3 4]
```

---

@title[Slice from Array examples]

##### Examples

```go
array := [3]int { 2, 3, 4 }

fmt.Println(array[1:])  // -> [3 4]
fmt.Println(array[:2])  // -> [2 3]
fmt.Println(array[1:3]) // -> [3 4]
fmt.Println(array[:])   // -> [2 3 4]
```
@[3]
@[4]
@[5]
@[6]

---

@title[Editing Slice Elements]

### Editing Slice Elements

```go
array := [3]int { 2, 3, 4 }
slice := array[1:3]

fmt.Println(array[1]) // -> 3

slice[0] = 10

fmt.Println(array[1]) // -> 10
```
@[6](Slice operating as reference)
@[8]

---

@title[Slice Functions]

### Slice Functions

```go
var array [11]int // int array of 0's
fullSlice := array[:]
partSlice := array[:5]
otherPart := array[5:7]

fmt.Println(fullSlice)      // -> [0 0 0 0 0 0 0 0 0 0 0]
fmt.Println(len(fullSlice)) // -> 11
fmt.Println(cap(fullSlice)) // -> 11

fmt.Println(partSlice)      // -> [0 0 0 0 0]
fmt.Println(len(partSlice)) // -> 5
fmt.Println(cap(partSlice)) // -> 11

fmt.Println(otherPart)      // -> [0 0]
fmt.Println(len(otherPart)) // -> 2
fmt.Println(cap(otherPart)) // -> 6
```
@[6-8](Full slice of array)
@[10-12](Whole array capacity)
@[14-16](Portion of array capacity)

---

@title[Appending to a Slice]

### Append

```go
var slice []int

fmt.Println(slice)      // -> []
fmt.Println(len(slice)) // -> 0
fmt.Println(cap(slice)) // -> 0

newSlice := append(slice, 4, 3, 4)

fmt.Println(newSlice)      // -> [4 3 4]
fmt.Println(len(newSlice)) // -> 3
fmt.Println(cap(newSlice)) // -> 4
```
@[3-5]
@[7]
@[9-11]

Note:

- Exponential growth of underlying array
- You cannot append to an array

---

@title[Copying a Slice]

### Copy

```go
src := []int { 4, 3, 4, 5 }
dest := make([]int, 2)

numberOfElementsCopied := copy(dest, src)

fmt.Println(dest)                   // -> [4 3]
fmt.Println(src)                    // -> [4 3 4 5]
fmt.Println(numberOfElementsCopied) // -> 2
```

Note:

- You can copy a slice to itself
- You can copy the contents of a string into a byte slice

---

@title[Slices and Make]

### Make

```go
slice := make([]int, 4)

fmt.Println(slice) // -> [0 0 0 0]
```

---

@title[Make's Abilities]

##### What can *make* do?

@ul
- Create a slice: `make([]type, length of slice, capacity of slice)`
- Create a map
- Create a channel
@ulend

---

@title[Examples of make-ing Slices]

### *make* and Slice Reassignment

```go
slice := make([]byte, 3, 10)
	
fmt.Println(slice)      // -> [0 0 0]
fmt.Println(len(slice)) // -> 3
fmt.Println(cap(slice)) // -> 10

slice = slice[:5]

fmt.Println(slice)      // -> [0 0 0 0 0]
fmt.Println(len(slice)) // -> 5
fmt.Println(cap(slice)) // -> 10

slice = slice[4:]

fmt.Println(slice)      // -> [0]
fmt.Println(len(slice)) // -> 1
fmt.Println(cap(slice)) // -> 6
```
@[7-11](Extended slice)
@[13-17](Shrunken slice)

---

@title[Functions]

### Functions

```go
func Foo(x int) int {
    return x + 5
}
```
@[1](Signature)
@[2](Body)

Note:

- No polymorphism

---

@title[Named Returns]

### Named Returns

```go
func Foo(x int) (result int) {
    result = x + 5
    return
}
```
@[2](Setting returned value)
@[3](Required return)

---

@title[Many Parameters]

### Many Parameters/Returns

```go
func Add(x int, y int) int {
    return x + y
}

func Subtract(x, y int) int {
    return x - y
}

func GetOptima(a, b, c, d int) (max, min int) {
    max = Max(a, b, c, d)
    min = Min(a, b, c, d)
    return
}
```
@[1 - 3]
@[5 - 7]
@[9 - 13]

---

@title[Variadic Arguments]

### Variadic Arguments

```go
func Add(nums ...int) int {
    sum := 0

    for i := 0; i < len(nums); i++ {
        sum += nums[i]
    }

    return sum
}

func PrintSum(nums []int) {
    fmt.Println(Add(nums...))
}
```
@[1](Variadic arguments)
@[12](Applying a slice)

---

@title[Variadic Arguments (cont.)]

```go
sum := Add(4, 5, 6, 6, 1)
```

---

@title[Maps]

### Maps

```go
// A new map instance
aMap := make(map[string]int)

// Inserting a value into the map
aMap["foo"] = 5
```

Note:

- Maps have a default value of nil
- Nil maps cannot have any keys added to them

---

@title[Retrieving From a Map]

### Interacting with map elements

```go
// Get the element
e, exists := aMap["something"] // e -> 0, exists = false

// Set the element
aMap["something"] = 4

// Get the element again
e, exists := aMap["something"] // e -> 4, exists = true

// Delete an element from a map
delete(aMap, "something")
```

---

@title[Map Literals]

### Map Literal

```go
m := map[string]int {
    "a": 65,
    "b": 66,
}
```

Note:

- Syntax gotcha with extra comma

---

@title[For Loop]

### For Loop

```go
for i := 0; i < 10; i++ {
    fmt.Println(i)
}

// -> 0
// -> 1
// -> 2
// -> 3
// ...
// -> 9
```

Note:

- Cannot put incrementor before variable

---

@title[Conditional For Loop]

### Whi - Er, For Loop

@ul[no-dot-list]
- <blockquote class="quote-text"><i class="fa fa-quote-left" aria-hidden="true"></i>C's while is spelled for in Go</blockquote>
@ulend

```go
s := 0

for s < 10 {
    s++
    fmt.Println(s)
}

// -> 0
// -> 1
// -> 2
// -> 3
// ...
// -> 9
```

---

@title[Infinite Loop]

### Infinite Loop

```go
condition := false

for {
    if (condition) {
        break;
    }

    condition = true
}
```

---

@title[Range Keyword]

### Range

```go
nums := []int { 2, 3, 4 }

for _, n := range nums {
    fmt.Println(n)
}

// -> 2
// -> 3
// -> 4
```

Note:

- Underscore to discard

---

@title[Range (cont.)]

### Range

```go
intMap := map[int]int {
    1: 23,
    2: 40,
    3: 98,
}

for k, v := range intMap {
    fmt.Println(k, v)
}

// -> 1 23
// -> 2 40
// -> 3 98
```

---

@title[Conditionals]

### Conditionals

```go
if (true) {
    doSomethingTrue()
}
```

---

@title[If-Else]

### If-Else

```go
if (true) {
    doSomethingTrue()
} else {
    doSomethingFalse()
}
```

---

@title[Initialization in a Conditional]

### Inline Initialization

```go
if bytesRead, err := rand.Read(array); err != nil {
    doSomethingWithError(err)
}
```

Note:

- Avoid using semi-colons when unnecessary

---

@title[The type Keyword]

### `Type`

Defines a new type.

---

@title[Aliasing]

### Aliasing

```go
type ByteSlice = []byte
```

Note:

- Serves as a true equal type

---

@title[Type Definition]

### Type Definitions

```go
type Hash []byte
type Index int
```
@[1]
@[2]

---

@title[Structs]

### Structs

```go
type Point struct {
    X int
    Y int
}
```

Note:

- X and Y are exported fields

---

@title[Non-Exported Fields]

### Exporting fields

```go
type IntArray struct {
    length int
    Beginning *int
}
```
@[2]
@[3]

Note:

- The capitalization is the exporting factor
- Exporting doesn't affect same-package definitions

---

@title[Struct Literals]

### Struct Literals

```go
// int_array.go
type IntArray struct {
    length int
    Beginning *int
}

// int_array_helper.go - same package as int_array.go
func NewIntArray(length int) IntArray {
    address := new(int) // Allocate new int, return pointer to it

    return IntArray { length, address }
}

// main.go - imports IntArray
func main() {
    memoryLocation := new(int) // Again: new int -> pointer

    // array1 := arrayPackage.IntArray { &memoryLocation }
    array1 := arrayPackage.IntArray {
        Beginning: memoryLocation,
    }

    array2 := arrayPackage.NewIntArray(20)

    array3 := arrayPackage.IntArray {}

    // ...
}
```
@[6-11](Has access to unexported fields)
@[13-28](No access to unexported fields)
@[11](Composite literal)
@[19-21](Composite literal with named fields)
@[25]("Zero value" literal)
@[18](Invalid literal)

Note:

- new will be explained later

---

@title[Pointers]

### Pointers

```go
func main() {
    // The zero-value for a pointer is always nil
    var ptr *int // -> nil
    
    // Point ptr to i's location in memory
    i := 12
    ptr = &i

    // Change the value at i (dereference)
    *ptr = 15

    fmt.Println(*ptr) // -> 15
    fmt.Println(i) // -> 15
}
```

@ul
- No pointer arithmetic
- Pointers cannot be set to an address
@ulend

Note:

- Pointers can behave as their own types
- Unsafe package - don't

---

@title[Receiver Methods]

### Receiver Methods

```go
type SliceStruct struct {
    b []byte
}

func (instance SliceStruct) SetSlice(slice []byte) {
    instance.b = slice
}
```
@[5-7](Receiver method for type SliceStruct)
@[5](instance = this)

---

@title[Receivers Don't Care About Calling Type]

### Receivers Don't Care About Calling Type

```go
func (value SomeType) Method() {
    // ...
}

func (pointer *SomeType) OtherMethod() {
    // ...
}

func main() {
    s := SomeType{}

    s.Method()      // SomeType.Method()
    s.OtherMethod() // (&SomeType).OtherMethod()
    
    sPtr := &s

    sPtr.Method()       // (*(*SomeType)).Method()
    sPtr.OtherMethod()  // (*SomeType).OtherMethod()
}
```
@[1-3](Value receiver method)
@[5-7](Pointer receiver method)
@[10](Value)
@[12](Value call)
@[13](Implicit indirect before dispatch)
@[15](Pointer to value)
@[17](Implicit dereference before dispatch)
@[18](Pointer call)

Note:

- You cannot "redeclare" methods
- Pointer receiver methods can be called with nil pointers

---

@title[new]

### `new`

##### Returns a pointer to a zeroed type

```go
func main() {
    p := new(int)

    // Is the same as
    var i int
    ptr := &i
}
```
@[2]
@[5-6]

Note:

- These two are exactly the same

---

@title[new with Structs]

### `new` with Structs

```go
type SomeType struct {
    Value int
}

func main() {
    p := new(SomeType) // p = &{0}

    // Is the same as
    ptr := &SomeType{} // ptr = &{0}
}
```
@[6]
@[9]

Note:

- All fields of type are zeroed

---

@title[Interfaces]

### Interfaces

```go
type IFoo interface {
    Bar(a int) int
}
```
@[1]
@[2]

Note:

- You may include unexported fields in interfaces, but be wary. Due to them being unexported, only structs in the same package can properly implement them.

---

@title[Implementing an Interface]

### Implementing an Interface

```go
type IDuck interface {
    Quack()
}

type Duck struct {}

func (duck Duck) Quack() {
    fmt.Println("Quack.")
}
```
@[1-3](An IDuck Quacks)
@[5]
@[7-9](Duck Quacks)

Note:

- Go is duck-typed. "If it walks like a duck and it quacks like a duck, then it must be a duck"
- "If it walks like a duck and quacks like a duck... well, then it's good enough"
- If it has the methods, then it's good enough

---

@title[Using Interfaces]

### Using Interfaces

```go
func main() {
    var duck IDuck = Duck{}     // Valid
    var duck2 IDuck = new(Duck) // Also valid

    duck.Quack()
    duck2.Quack()
}
```
@[2](Instance)
@[3](Pointer to an instance)
@[5](Calling a value receiver)
@[6](Type conversion happens automatically)

Note:

- Nil underlying types work
- Gotcha with pointers getting dereferenced automatically

---

@title[Pointer Receivers and Interfaces Gotcha]

### A Gotcha with Pointers and Interfaces

```go
func (duck *Duck) Quack() {
    fmt.Println("Quack.")
}

func main() {
    var duck IDuck = new(Duck)  // Valid
    var duck2 IDuck = Duck{}    // Cannot use Duck literal as type IDuck
}
```
@[1](Pointer receiver)
@[6]
@[7]

---

@title[Nil Interfaces]

### Nil Interfaces

```go
func main() {
    var duck IDuck

    duck.Quack() // Runtime error
}
```

Note:

- Interface is nil, not just underlying type

---

@title[Type Assertion]

### Type Assertion

```go
func main() {
    var duck IDuck = Duck{}

    assertedDuck, ok := duck.(Duck)

    assertedDuck = duck.(Duck)

    assertedDuck = duck.(string) // Runtime error
}
```
@[4](Checking if assertion succeeded)
@[6](Confident assertion)
@[8](Causes a panic)

Note:

- Similar to C#'s "as"
- Only interface types are asserted

---

@title[Type Conversion]

### Type Conversion

```go
x := 4
y := float32(x)
```

---

@title[Type Switches]

### Type Switches

```go
func Foo(i interface{}) {
    switch value := i.(type) {
        case string:
            fmt.Println("string")
        case int:
            fmt.Println("int")
        default:
            fmt.Println("unknown")
    }
}
```
@[1](Empty interface)
@[2](Type switch)
@[3]
@[5]
@[7]

Note:

- Logically only interfaces may be "switched" 

---

@title[Errors]

### Errors

```go
type error interface {
    Error() string
}
```

Note:

- error is simply an interface type

---

@title[New() Errors]

### New() Errors

```go
import (
    "errors"
    "fmt"
)

func main() {
    fmt.Println(errors.New("Some error"))
}
```

---

@title[Custom Errors]

### Custom Errors

```go
type ReadError struct { }

func (e *ReadError) Error() string {
    return "Failed to read from requested resource."
}
```

---

@title[Panic]

### panic

```go
// Assume data is defined
s := SomeType{}
err := json.Unmarshal(data, s)

if (err != nil) {
    panic(err)
}
```
@[3](Possible error)
@[5-7]
@[6](panic = throw)

Note:

- It's idiomatic to return errors instead

---

@title[Defer]

### defer

```go
func Bar(i int) int {
    defer fmt.Println("i is:", i)

    i++

    return i
}

// > Bar(4)
// -> 5
// -> i is: 4
```
@[2](Function call)

Note:

- Defer runs after function terminates, but it's arguments are evaluated immediately
- Defer may be thought of as a finally block

---

@title[Recover]

### recover

```go
func f() {
    defer func() {
        if r := recover(); r != nil {
            fmt.Println("We recovered!")
        }
    }()

    panic("Some error has occurred") // Execution of f stops here

    fmt.Println("Finished executing f")
}

func main() {
    f()
    // -> We recovered!
}
```
@[8](Panic)
@[3](Call to recover)

Note:

- recover() returns nil if there is no panic; otherwise, it returns the value given to panic
- Only callable from defer's

---

@title[Returning Errors]

### Returning Errors

```go
import (
    "errors"
    "math"
)

func Increment(i int8) (int, error) {
    if (i == math.MaxInt8) {
        return 0, errors.New("Increment would cause overflow.")
    }

    return i + 1, nil
}
```
@[8](Error returned)
@[11](Nil returned)

Note:

- Proper way to deal with exceptional behavior

---

@title[Go keyword]

### `go`

```go
func PrintMany(times int, msg string) {
  for i := 0; i < times; i++ {
    PrintMessage(msg)
  }
}

func PrintMessage(msg string) {
  fmt.Println(msg)
}

func main() {
    go PrintMany(5, "First async function")

    fmt.Println("main function printing")

    go PrintMessage("Another async function")

    fmt.Scanln()
}
/*
 -> main function printing
 -> First async function
 -> Another async function
 -> First async function
 -> First async function
 -> First async function
 -> First async function
*/
```
@[1-5]
@[7-9]
@[11-19]

Note:

- BIG NOTE: if main terminates, all other go routines are killed, no matter their state

---

@title[CSP Intro]

### CSP Concurrency

@ul
- Communicating Sequential Processes (Tony Hoare, 1978)
- Erlang, Concurrent ML, Newsqueak
- Go communicates by *channels*, not process names
- Concurrency **is not** parallelism
@ulend

---

@title[Goroutines]

### Goroutines

@ul
- Stack is automatically dealt with
- Goroutines are "multiplexed" on to available threads
- Encouraged (where practical)
@ulend

---

@title[Channels]

### Channels

```go
var chan1 chan int
chan2 := make(chan int)

i := <-chan1 // Get (wait for) value from chan1
chan2 <- i   // Give chan2 a value
```

Note:
- Sending and receiving are blocking for synchronization purposes

---

@title[Single-Direction Channels]

### Single-Direction Channels

```go
var inChan chan<- int
var outChan <-chan int
```

Note:

- Channel always points left

---

@title[Close]

### `close`

```go
c := make(chan int)

c <- 4
c <- 7

close(c) // Closes this channel, telling other references that it cannot be "spoken with"
```

Note:

- Go expects a goroutine to running when a channel is read; this is important to understand close's use

---

@title[Channels in Goroutines]

### Channels in Goroutines

```go
func SimpleExample(c chan int) {
    for i := 0; i < 10; i++ {
        c <- i
    }

    close(c)
}

func main() {
    channel := make(chan int)

    go SimpleExample(channel)

    for i := range channel {
        // Do something with i
    }
}
```
@[1](Functon that takes a channel)
@[2-4](Loop through numbers 0-9, sending them to c)
@[6](Close the channel)
@[10](Make a new channel)
@[12](Spin up new goroutine running SimpleExample with our new channel)
@[14](Set i to every value sent to channel - until it is closed)

---

@title[Select]

### `select`

```go
import "fmt"

func EndlessIncrement() (<-chan int, chan<- bool) {
	c := make(chan int)
	quit := make(chan bool)
	
	go func() {
		for i := 0; true; i++ {
			select {
			case c <- i:
			case <-quit:
				close(c)
				close(quit)
				return
			}
		}
	}()
	
	return c, quit
}

func main() {
	channel, cancel := EndlessIncrement()
	
	for i := range channel {
		if i > 100 {
			cancel <- true
			break
		}
		
		fmt.Println(i)
	}
}
```
@[3](Returns a read-only int channel and a write-only bool channel for canceling the loop)
@[4-5](Making our channels)
@[8](Loop forever)
@[10-11](Waits on one of these two cases to be available)
@[12-13](Close up our channels)
@[19](Return our new channels)
@[23](Get new channels from method)
@[26-29](Condition that we want to cancel the goroutine)

Note:

- Also covering cancellation token
- Default is chosen if the no other conditions are available

---

@title[Generator Pattern]

### Generator Pattern

```go
import (
	"fmt"
	"time"
)

func PrintMany(times int, msg string) <-chan string {
	returnedChannel := make(chan string)
	
	go func() {
		for i := 0; i < times; i++ {
			time.Sleep(time.Second / 2)
			returnedChannel <- fmt.Sprintf("%d: %s", i, msg)
		}
		
		close(returnedChannel)
	}()	
	
	return returnedChannel
}

func main() {
    c := PrintMany(10, "Async")
	
	for i := range c {
		fmt.Println(i)
	}
	
	fmt.Println("Done!")
}
```
@[6](Function returning a read-only string channel)
@[7](Make the returned channel)
@[9-16](Spin up a new goroutine)
@[18](Return the read-only channel)
@[22](Gets a new channel)
@[24](Wait until channel closes)
@[10-13](Wait a half second, then send formatted string to channel)
@[15](Finally, close the channel)

---

@title[Fan-in]

### Fan-in

```go
import "sync"

func FanIn(channels ...<-chan string) <-chan string {
    c := make(chan string)
    
    var waitGroup sync.WaitGroup
    waitGroup.Add(len(channels))

    for _, channel := range channels {
        go func(_channel <-chan string) {
            for elem := range _channel {
                c <- elem
            }

            waitGroup.Done()
        }(channel)
    }

    go func() {
        waitGroup.Wait()
        close(c)
    }()

    return c
}
```
@[1](Need sync.WaitGroup)
@[3](FanIn takes any number of read-only string channels)
@[6-7](New sync.WaitGroup that waits on the number of channels we have)
@[9](For each channel...)
@[10](Spin a new goroutine that takes a read-only string channel)
@[11-13](Send all the channel's values to our master channel)
@[15](Decrement the amount of waiters)
@[16](Calling the func with a channel)
@[19-22](Wait to close master channel)
@[24](Return master channel)

---

@title[Import]

### `import`

```go
import "fmt"
import m "math"
```

---

@title[Dot Import]

### Dot Import

```go
import (
    "fmt"
    . "math"
)

func main() {
    v := Sqrt(4)

    fmt.Println(v)
}
```
@[3](Imports all fields, methods, etc. globally)

---

@title[Underscore Import]

### Underscore Import

```go
import (
    _ "somePackage"
)
```

---

@title[How are Packages Arranged?]

### How are Packages Arranged?

@ul
- GOROOT: usually the location of the Go SDK
- GOPATH/src: the directory with your source code
- GOPATH/vendor: generally used for third party packages
@ulend